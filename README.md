# Activity Filter Service Rules

The following is a list of rules concerning the KDE Activity Filter Service which must be adhered to by all rules processed by the system.

## Personal Rules

These should be created in the personal/ folder, with one file for each person's own rules.
Each file should follow the user's username (eg: for scripty, it would be personal/scripty.yaml)

People may set these up at their discretion for both the Commits and Bugzilla categories, and aren't limited with regards to the rules they create (within reason).
For Gitlab, users should subscribe to the projects/groups they're interested in within the Gitlab interface, as this enables use of Reply by Email functionality, as well as offering a more personalised experience.

While users are welcome to use alternative addresses (ones that are not their primary address) under no circumstances is for another person's address to be used in Activity Filter.

## Mailing List Rules

As mailing lists have a substantial reach, with lists sometimes numbering in hundreds to thousands of subscribers, mailing lists may not be setup within Activity Filter without first getting the consent of the list in question.

This must be done by opening a public discussion on the exact subscription that the list will receive from Activity Filter on the list itself, and asking if people are happy for this to be setup.
It is recommended that the precise Activity Filter rules be discussed as part of this process.

This discussion period must be left open for a minimum of 72 hours.

In the event that any objections are raised, these must be resolved before Activity Filter rules are put in place.
Should the objection be unresolvable, then lists may not proceed with setting up the Activity Filter rules in question.

When committing these Activity Filter rules, a link to the mailing list archives for the discussion must be included for future reference in the event complaints are raised later.
These rules should be committed to the lists/ folder, using the name of the mailing list (with the kde.org component removed) as the name of the file (eg: kde-frameworks-devel would be lists/kde-frameworks-devel.yaml)

For mailing lists, there are no limitations as to the rules that are setup, with rules able to be setup for Commits, Bugs and GitLab activity.

## Example rules

```yaml
- name: "Frameworks MR Notifications"
  subscribes: "kde-frameworks-devel@kde.org"
  to: merge_requests
  where:
    project: 'frameworks/.*'


- name: "Frameworks Task Notifications"
  subscribes: "kde-frameworks-devel@kde.org"
  to: tasks
  where:
    project: 'frameworks/.*'


- name: "KMyMoney Commits by Scripty"
  subscribes: "kmymoney-commits@kde.org"
  to: commits
  where:
    project: 'office/kmymoney'
    branch: 'master'
    author: 'scripty'

- name: "Marble Bugs List"
  subscribes: "marble-bugs@kde.org"
  to: bugs
  where:
    product: 'marble'
    component: 'general'
```
